import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { UserService } from 'src/app/Services/user.service';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-edit-user',
  templateUrl: './edit-user.component.html',
  styleUrls: ['./edit-user.component.scss'],
})
export class EditUserComponent implements OnInit {
  userId: string = '';
  userDetails: any;
  dataLoaded: boolean = false;
  editUserForm: FormGroup = new FormGroup({});
  constructor(
    private activatedRoute: ActivatedRoute,
    private userService: UserService,
    private formBuilder: FormBuilder,
    private _snackBar: MatSnackBar
  ) {}

  ngOnInit(): void {
    this.dataLoaded = false;
    this.activatedRoute.params.subscribe((data) => {
      this.userId = data['id'];
    });
    if (this.userId != '')
      this.userService
        .viewUsers(this.userId)
        .toPromise()
        .then((data) => {          
          this.userDetails = data;
          console.log( "this.userDetails", this.userDetails);

          this.editUserForm = this.formBuilder.group({
            username: new FormControl(this.userDetails.data.username),
            email: new FormControl(this.userDetails.data.email),
            phoneNumber: new FormControl(this.userDetails.data.phoneNumber),
            bloodGroup: new FormControl(this.userDetails.data.bloodGroup),
            password: new FormControl(this.userDetails.data.password),
          });
          this.dataLoaded = true;
        })
        .catch((err) => {
          console.log('errr', err);
        });
  }
  updateUser() {
    this.userService.updateUser(this.editUserForm.value, this.userId).subscribe(
      (data:any) => {
        if(data.error==false){
          console.log('data',data);
        
          this._snackBar.open('  User update Successfully');
        }else{
          this._snackBar.open(data.message);
        }
      },
      (err) => {
        this._snackBar.open('Unable to update a new user');
      }
    );
  }
}
