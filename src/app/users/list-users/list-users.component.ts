import { Component, OnInit, Inject } from '@angular/core';
import { Observable } from 'rxjs';
import { UserService } from 'src/app/Services/user.service';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ViewUserComponent } from '../view-user/view-user.component';
import { MatSnackBar } from '@angular/material/snack-bar';
import { LayoutModule } from 'src/app/layout/layout.module';

@Component({
  selector: 'app-list-users',
  templateUrl: './list-users.component.html',
  styleUrls: ['./list-users.component.scss'],
})
export class ListUsersComponent implements OnInit {
  dataSource: any;
  userDetails: any;
  constructor(
    private userService: UserService,
    private dialog: MatDialog,
    private _snackBar: MatSnackBar
  ) {}
  displayedColumns: string[] = [
    'demo-position',
    'demo-name',
    'demo-weight',
    'demo-symbol',
    'demo-actions',
  ];
  // dataSource = ELEMENT_DATA;
  ngOnInit(): void {
    this.getUserData();
  }
  getUserData() {
    this.userService.listUsers().subscribe((data:any) => {
      this.dataSource = data.data;
      console.log('this. this.dataSource', this.dataSource);
      
      
    });
  }
  async openDialog(userId: any) {
    
    await this.userService.viewUsers(userId).subscribe((data:any) => {
      console.log('userId',data.pageValue);

      this.userDetails = data;
      if (this.userDetails) {
        this.dialog.open(ViewUserComponent, {
          width: '2500px',
          data: { pageValue: this.userDetails },
        });
      }
    });
  }
  async delete(userId: any) {
    await this.userService.deleteUser(userId).subscribe(
      (data) => {
        alert('Are you sure do you want to delete this employee');
        this._snackBar.open(' user deleted successfully');
        this.getUserData();
        console.log( this.getUserData());
        
      },
      (err) => {
        this._snackBar.open(' something went wrong ang unable to delete user');
      }
    );
  }
}
