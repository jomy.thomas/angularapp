import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LoginComponent } from './login/login.component';
import { SignUpComponent } from './signup/signup.component';
import { BrowserModule } from '@angular/platform-browser';
import {RouterModule} from '@angular/router';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { HomeComponent } from './home/home.component';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import {MatIconModule} from '@angular/material/icon';
import { LayoutModule } from '../layout/layout.module';
import { ProfileComponent } from './profile/profile.component';
import {MatTableModule} from '@angular/material/table';

@NgModule({
  declarations: [
    LoginComponent,
    SignUpComponent,
    PageNotFoundComponent,
    HomeComponent,
    ProfileComponent
  ],
  imports: [
    CommonModule,
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    MatFormFieldModule,
    MatInputModule,
    MatIconModule,
    LayoutModule,
    MatTableModule,
  ]
})
export class CustomersModule { }
