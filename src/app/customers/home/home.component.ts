import { Component, OnInit, Inject } from '@angular/core';
import { UserService } from 'src/app/Services/user.service';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {
  // userId: string = '';
  // userDetails: any;
  // Details: any;

  // dataLoaded: boolean = false;
  // editUserForm: FormGroup = new FormGroup({});
  // constructor(
  //   private activatedRoute: ActivatedRoute,
  //   private userService: UserService,
  //   private formBuilder: FormBuilder,
  //   private _snackBar: MatSnackBar,
  //   private router: Router
  // ) {}
  // //  navigation = this.router.getCurrentNavigation();

  // ngOnInit(): void {
  //   this.dataLoaded = false;
  //   this.Details = this.userService.getEmail();
  //   this.activatedRoute.queryParams.subscribe((data) => {
  //     console.log('##########', data);

  //     this.userId = data['id'];
  //   });
  //   if (this.userId != '')
  //     this.userService
  //       .viewUsers(this.userId)
  //       .toPromise()
  //       .then((data) => {
  //         this.userDetails = data;
  //         this.editUserForm = this.formBuilder.group({
  //           username: new FormControl(this.userDetails.username),
  //           email: new FormControl(this.userDetails.email),
  //           phoneNumber: new FormControl(this.userDetails.phoneNumber),
  //           bloodGroup: new FormControl(this.userDetails.bloodGroup),
  //           password: new FormControl(this.userDetails.password),
  //         });
  //         this.dataLoaded = true;
  //       })
  //       .catch((err) => {
  //         console.log('errr', err);
  //       });
  // }
  // updateUser() {
  //   this.userService.updateUser(this.editUserForm.value, this.userId).subscribe(
  //     (data) => {
  //       this._snackBar.open('  User update Successfully');
  //     },
  //     (err) => {
  //       this._snackBar.open('Unable to update a new user');
  //     }
  //   );
  // }
  userId: string = '';
  dataSource: any;
  userDetails: any;
  constructor(
    private userService: UserService,
    private dialog: MatDialog,
    private _snackBar: MatSnackBar,
    private activatedRoute: ActivatedRoute,

  ) {}
  displayedColumns: string[] = [
    'demo-position',
    'demo-name',
    'demo-weight',
    'demo-symbol',
    'demo-actions',
  ];
  // dataSource = ELEMENT_DATA;
  ngOnInit(): void {
    this.getUserData();
  }
  getUserData() {
    // this.userService.listUsers().subscribe((data) => {
    //   this.dataSource = data;
    // });
    this.activatedRoute.queryParams.subscribe((data) => {
      console.log('##########', data);

      this.userId = data['id'];
      console.log('##########   this.userId',    this.userId);

    });
    this.userService.viewUsers(this.userId).subscribe((data) => {
      this.dataSource = data;
      console.log('########## this.dataSource ', this.dataSource );
    });
  }
  
}
