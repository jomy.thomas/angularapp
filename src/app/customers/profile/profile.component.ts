import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { UserService } from 'src/app/Services/user.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {

  userId: string = '';
  userDetails: any;
  Details: any;

  dataLoaded: boolean = false;
  editUserForm: FormGroup = new FormGroup({});
  constructor(
    private activatedRoute: ActivatedRoute,
    private userService: UserService,
    private formBuilder: FormBuilder,
    private _snackBar: MatSnackBar,
    private router: Router
  ) {}
  //  navigation = this.router.getCurrentNavigation();

  ngOnInit(): void {
    this.dataLoaded = false;
    this.Details = this.userService.getEmail();
    this.activatedRoute.params.subscribe((data) => {
      console.log('##########', data);

      this.userId = data['id'];
    });
    if (this.userId != '')
      this.userService
        .viewUsers(this.userId)
        .toPromise()
        .then((result:any) => {
          console.log('data',result);
          
          this.userDetails = result.data;
          this.editUserForm = this.formBuilder.group({
            username: new FormControl(this.userDetails.username),
            email: new FormControl(this.userDetails.email),
            phoneNumber: new FormControl(this.userDetails.phoneNumber),
            bloodGroup: new FormControl(this.userDetails.bloodGroup),
            password: new FormControl(this.userDetails.password),
          });
          this.dataLoaded = true;
        })
        .catch((err) => {
          console.log('errr', err);
        });
  }
  updateUser() {
    this.userService.updateUser(this.editUserForm.value, this.userId).subscribe(
      (data:any) => {
        if(data.error==false){
          console.log('data',data);
        
          this._snackBar.open('  User update Successfully');
        }else{
          this._snackBar.open(data.message);
        }
       
      },
      (err) => {
        this._snackBar.open('Unable to update a new user');
      }
    );
  }

}
