import { Component, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { Router } from '@angular/router';
import { UserService } from 'src/app/Services/user.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup = new FormGroup({});
  dataSource: any;
  loginUserDetails: any;
  loginAdminDetails: any;


  role: any = '';

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private userService: UserService,

  ) {}

  ngOnInit(): void {
    if (this.userService.isLoggedIn()) {
      this.router.navigate(['users']);
    }
    if (this.userService.isLoggedInUser()) {
      this.router.navigate(['home']);
    }
    this.loginForm = this.formBuilder.group({
      email: new FormControl('', [Validators.required, Validators.email]),
      password: new FormControl('', [
        Validators.required,
        Validators.minLength(4),
      ]),
    });
  }
  onLogin() {
    if (this.loginForm.valid) {
      console.log('this.loginForm.value',this.loginForm.value);
      
      this.userService.login(this.loginForm.value).subscribe(
        (result) => {
          console.log("result",result);
          this.loginAdminDetails = result
         if(this.loginAdminDetails.admin==true){
          this.userService.setToken('fiauyeiuyaiyery623698yafhauiehf93r69admin');
          this.router.navigate(['users']);
         }else{
          if(this.loginAdminDetails.error==true){
            alert(this.loginAdminDetails.message)
          }
          this.loginUserDetails = this.loginAdminDetails.data.id
          if (this.loginUserDetails) {
            console.log('this.loginUserDetails',this.loginUserDetails);
            
            this.userService.setToken(
              this.loginAdminDetails.token
            );
            // this.toastr.success(' welcome');
            this.router.navigate(['home'], {
              queryParams: { id: this.loginUserDetails },
            });

            this.loginForm.reset();
          } else {
            alert('user is not found');
          }
         }
        },
        (err: Error) => {
          alert(err.message)
          // this.userService.listUsers().subscribe(
          //   (res) => {
          //     this.dataSource = res;
          //     if (this.dataSource.length) {
          //       this.dataSource.find((e: any) => {
          //         if (
          //           e.email === this.loginForm.value.email &&
          //           e.password === this.loginForm.value.password
          //         ) {
          //           console.log('#####', e);
          //           return (this.loginUserDetails = e.id);
          //         }
          //       });
          //       console.log(' this.loginUserDetails', this.loginUserDetails);

          //       if (this.loginUserDetails) {
          //         this.userService.setToken(
          //           'fiauyeiuyaiyery623698yafhauiehf93r69'
          //         );
          //         // this.toastr.success(' welcome');
          //         this.router.navigate(['home'], {
          //           queryParams: { id: this.loginUserDetails },
          //         });

          //         this.loginForm.reset();
          //       } else {
          //         alert('user is not found');
          //       }
          //     } else {
          //       alert('user is not found');
          //     }
          //   },
          //   (err) => {
          //     alert('something went wrong');
          //   }
          // );
        }
      );
    }
  }
}
