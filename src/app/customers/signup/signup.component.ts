import { Component, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { UserService } from 'src/app/Services/user.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';


@Component({
  selector: 'app-sign-up',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss'],
})
export class SignUpComponent implements OnInit {
  addUserForm: FormGroup = new FormGroup({});

  constructor(
    private formBuilder: FormBuilder,
    private userService: UserService,
    private _snackBar: MatSnackBar,
    private router: Router,
  ) {}

  ngOnInit(): void {
    if (this.userService.isLoggedIn()) {
      this.router.navigate(['users']);
    }
    if (this.userService.isLoggedInUser()) {
      this.router.navigate(['home']);
    }
    this.addUserForm = this.formBuilder.group({
      username: new FormControl('', [
        Validators.required,
        Validators.minLength(3),
      ]),
      email: new FormControl('', [Validators.required, Validators.email]),
      phoneNumber: new FormControl('', [
        Validators.required,
        Validators.minLength(10),
      ]),
      bloodGroup: new FormControl('', [
        Validators.required,
        Validators.minLength(1),
      ]),
      password: new FormControl('', [
        Validators.required,
        Validators.minLength(4),
      ]),
    });
  }
  createUser() {
    console.log('this.addUserForm.value', this.addUserForm.value);

    this.userService.addUser(this.addUserForm.value).subscribe(
      (data:any) => {
        console.log(data);
        if(data.error==false){
        alert(' new User Create Successfully');
        this.addUserForm.reset();
        this.router.navigate(['login']);
        }
        if(data.error==true){
        alert(data.message);
        }
        
      },
      (err) => {
        alert('Unable to create a new user');
      }
    );
  }
}
