import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { Observable, of, throwError } from 'rxjs';
import jwt_decode from "jwt-decode";
@Injectable({
  providedIn: 'root',
})
export class UserService {
 readonly baseUrl: string = 'http://localhost:5000/';
 jwtToken: any;
    decodedToken: any;
  constructor(private http: HttpClient, private router: Router) {}

  listUsers() {
    return this.http.get(this.baseUrl + 'api/list-users');
  }
  viewUsers(id: string) {
    return this.http.get(this.baseUrl + `api/get-user-details/${id}`);
  }
  addUser(userObj: any) {
    console.log('userObj', userObj);
    return this.http.post( this.baseUrl + 'api/signup', userObj);
  }
  deleteUser(id: any) {
    return this.http.delete(this.baseUrl + `api/delete-user-details/${id}`);
  }
  updateUser(userObj: any, id: any) {
    return this.http.put(this.baseUrl + `api/update-user-details/${id}`, userObj);
  }
  setToken(token: string): void {
    localStorage.setItem('token', token);
  }
  setEmail(email: string): void {
    localStorage.setItem('email', email);
  }
  getEmail(): string | null {
    return localStorage.getItem('email');
  }
  getToken(): string | null {
    return localStorage.getItem('token');
  }
  isLoggedIn() {
    return this.getToken() == 'fiauyeiuyaiyery623698yafhauiehf93r69admin';
  }
  isLoggedInUser() {
    
    console.log('token', this.getToken());
    this.jwtToken = this.getToken() 
    if(this.jwtToken){
      return  this.decodedToken = jwt_decode(this.jwtToken);

    }
  }
  loggedOut() {
    localStorage.removeItem('token');
    this.router.navigate(['login']);
  }
  // login({ email, password }: any): Observable<any> {
  //   if (email === 'admin@gmail.com' && password === 'admin@123') {
  //     this.setToken('fiauyeiuyaiyery623698yafhauiehf93r69admin');
  //     return of({ name: 'admin', email: 'admin@gmail.com' });
  //   }
  //   return throwError(new Error('failed to login'));
  // }
  login({ email, password }: any) {
    console.log('userObj', {email,password});
    return this.http.post( this.baseUrl + 'api/login', {email,password});
  }
}
