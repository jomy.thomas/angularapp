import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './header/header.component';
import { SideBarComponent } from './side-bar/side-bar.component';
import { FooterComponent } from './footer/footer.component';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { RouterModule } from '@angular/router';
import { SideBarUserComponent } from './side-bar-user/side-bar-user.component';
@NgModule({
  declarations: [HeaderComponent, SideBarComponent, FooterComponent, SideBarUserComponent],
  imports: [
    CommonModule,
    MatToolbarModule,
    MatIconModule,
    MatListModule,
    MatSnackBarModule,
    RouterModule,
  ],
  exports: [HeaderComponent, SideBarComponent, FooterComponent,SideBarUserComponent],
})
export class LayoutModule {}
