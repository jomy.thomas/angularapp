import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './customers/home/home.component';
import { LoginComponent } from './customers/login/login.component';
import { PageNotFoundComponent } from './customers/page-not-found/page-not-found.component';
import { ProfileComponent } from './customers/profile/profile.component';
import { SignUpComponent } from './customers/signup/signup.component';
import { AuthGuard } from './guards/auth.guard';
import { UserGuardsGuard } from './guards/user-guards.guard';
import { AddUserComponent } from './users/add-user/add-user.component';
import { DeleteUserComponent } from './users/delete-user/delete-user.component';
import { EditUserComponent } from './users/edit-user/edit-user.component';
import { ListUsersComponent } from './users/list-users/list-users.component';
import { ViewUserComponent } from './users/view-user/view-user.component';

const routes: Routes = [
  {
    path: 'users',
    canActivate: [AuthGuard],

    children: [
      { path: 'create', component: AddUserComponent },
      { path: '', component: ListUsersComponent },
      { path: 'list', component: ListUsersComponent },
      { path: 'view/:id', component: ViewUserComponent },
      { path: 'edit/:id', component: EditUserComponent },
      { path: 'delete/:id', component: DeleteUserComponent },
    ],
  },
  { path: 'login', component: LoginComponent },
  { path: 'sign-up', component: SignUpComponent },
  { path: 'home', component: HomeComponent, canActivate: [UserGuardsGuard] },
  { path: 'home/:id', component: HomeComponent, canActivate: [UserGuardsGuard] },
  { path: 'profile/:id', component: ProfileComponent, canActivate: [UserGuardsGuard] },
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  { path: '**', component: PageNotFoundComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
